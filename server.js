const http= require("http");
const fs =require('fs');
const uuid=require('uuid4');
const port=process.env.PORT || 5432;


const server = http.createServer((req,res) => {
    if(req.method=='GET'){
        if(req.url=='/html'){
            fs.readFile('./resources/htmlDocument.html','utf-8',(err,data)=> {
                if(err){
                    console.log(err);
                }else{
                    res.writeHead("200",{ 'Content-type' : 'text/html'});
                    res.write(data);
                    res.end();
                }
            });
        }else if(req.url=='/json'){
            fs.readFile('./resources/jsonDocument.json','utf-8',(err,data)=> {
                if(err){
                    console.log(err);
                }else{
                    data=JSON.parse(data);
                    data=JSON.stringify(data,null,2);
                    res.write(data);
                    res.end();
                }
            });
        }else if(req.url=='/uuid'){
            fs.readFile('./resources/jsonDocument.json','utf-8',(err,data)=> {
                let id=uuid();
                res.write(JSON.stringify(id,null,2));
                res.end();
                
            });
        }else if(req.url.includes('/status')){
            let code=req.url.slice(8)
            res.writeHead(code, { 'Content-Type': 'application/json' });
            res.end(code);
        }else if(req.url.includes('/delay')){
            let time=Number(req.url.slice(7));
            setTimeout(() => {
                res.writeHead(200);
                res.end("After"+time+ "seconds.");
            }, time*1000);

        }
    }
});

server.listen(port,()=>{
    console.log("listening");
});